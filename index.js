// console.log("hello world");

// [Section] Functions
	// Functions in javascript are line/blocks of codes that our device/application to perform a specific task when called/invoked.
	// it prevents repeating lines/blocks of codes that perform the same task/function.

	// Function Declarations
		// function statement is the definition of a function keyword.
		
	/*
		Syntax:
			function functionName(){
				code block (statement)
			}

		- function keyword - used to define a javascript function
		- functionName - name of the function, which will be used to call/invoke the function
		- function block({}) - indicates the function body
	*/

	function printName(){
		console.log("My name is John");
	}

// Function Invocation
	// The code block and statement inside  afunction is not immediately executed when the function is define
	// 
	// This run/execute the code block inside the function
	printName();

	declaredFunction(); // we cannot invoke a function that we have not declared/define yet.

// [Section] Function Declarations vs Function Expressions
	
	// Function Declaration
		// function can be created by using function keyword and adding a function time
		// "saved by later used"
		// Declared functions can be "hoisted" as long as a function has been defined
			// Hoisting is JS behavior for certain variables (var) and functions to run or use them before declarations.

	function declaredFunction(){
		console.log("Hello world from declaredFunction()");
	}

	// Function Expression
		// A function can also be stored in a variable.

		/*
			Syntax:
				let/const variableName = function(){
					//code block (statement)
				}

				-function(){} - anonymous function, a function without a name
		*/

		// variableFunction(); // error - function expression, being stored in a let/const variable, cannot be hoisted

		let variableFunction = function(){
			console.log("Hello Again");
		}

		variableFunction();

		// function declaration is a global scope you can access any part of the code while function expression is only after the function.

		// We create also a function expression with a named function.

		let funcExpression = function funcName(){
			console.log("hello from the other side.");
		}

		// funcName(); // funcName() is not define
		funcExpression(); // to invoke the function, we invoked it by its "variable name" and not by its function name.

		// Reassign declared functions and function expression to a new anonymous function.

		declaredFunction = function(){
			console.log("Updated declaredFunction")
		}

		declaredFunction();

		funcExpression = function(){
			console.log("updated funcExpression")
		}

		funcExpression();

		// we cannot re-assign a function expression initialized with a const.

		const constantFunc =  function(){
			console.log("Initialized with const");
		}

		constantFunc();


		// This will result to reassignment error
		// constantFunc = function(){
		// 	console.log("Cannot be reassigned");
		// }
		// constantFunc();

// [Section] Function Scoping
		/*
			Scope is the accessibility (visibility) of variables

			JavaScript Variables has a 3 types of scope:
			1. global scope
			2. local/block scope
			3. function scope

		*/

		// Global Scope
			// variable can be access any where from the program

		let globalVar = "Mr. WorldWide";

		// Local Scope
			// Variables declared inside a curly bracket ({}) can only be accessed locally.

		// console.log(localVar);

		{
			// var localVar = "armando perez";
			let localVar = "armando perez";

		}

		console.log(globalVar);
		// console.log(localVar); // result in error cannot be accessed outside its code block.

		// Function Scope
			// Each Function creates a new scope.
			// Variables defined inside a function are not accessible from outside the funtion.

		function showNames(){
			// function scope variable
			var functionVar = "joe";
			const functionConst = "john";
			let functionLet = "joey";

			console.log(functionVar);
			console.log(functionConst);
			console.log(functionLet);
		}

		showNames();

		// console.log(functionVar);
		// console.log(functionConst);
		// console.log(functionLet);

		function myNewFunction(){
			let name = "jane";

			function nestedFunc(){
				let nestedName = "john";
				console.log(name);
			}

			// console.log(nestedName);
			nestedFunc();
		}

		myNewFunction();

		// nestedFunc(); // result to an error.

		// Global Scope Variable

		let globalName = "alex";

		function myNewFunction2(){
			let nameInside = "renz"
			console.log(globalName);
			console.log(nameInside);
		}

		myNewFunction2();
		// console.log(nameInside); //only accessible on the function scope

// [Section] Using Alert() and prompt()
	// alert() allows us to show small window at the top of our browser page to show information to our users.

	// alert("hello world"); // this will run imediately when the page reloads.

	// you can use an alert() to show a message to the user from a later function invocation.

	// function showSampleAlert(){
	// 	alert("hello,User");
	// }

	// showSampleAlert();
	console.log("I will only log in the console when the alert is dismissed.");

	// Notes on the use of alert():

		// Show only an alert() for short dialogs/messages to the user
		//  Do not overuse alert() because the program has to wait for it to be dismissed before it continues

	// Prompt() allows us to show a small window at the top of the browser to gather user input.
	// the input from the prompt() will returned as a "String" once the user dismissed the window.

	/*
		Syntax: let/const variableName = prompt("<dialogInString>");
	*/

	// let name = prompt("Enter your name: ");
	// let age = prompt("Enter you age: ");

	// console.log(typeof age);

	// console.log("Hello, I am "+name+" and I am "+age+" years old.");

	// let sampleNullPrompt = prompt("Do not enter Anything");
	// console.log(sampleNullPrompt)
	// prompt() return an "empty string" ("") when there is no user input and we have clicked okay or "null" if the user cancels the prompt.


	function printWelcomeMessage(){
		let name = prompt("Enter your name: ");

		console.log("hello, "+name+"! Welcome to my page");
	}
	printWelcomeMessage();

	// [Section] Function Naming Conventions

		// 1. Function names should be definitive of the task it will perform. It usually contains a verb.
			// functions also allow the 

			function getCourses(){
				let courses = ["science 101", "Math 101", "English 101"];
				console.log(courses);
			}
			 getCourses();

		// 2. Avoid generic names to avoid confusion within your code.

			function get(){
			 	let name = "jamie";
			 	console.log(name)
			}
			 get();

		// 3. Avoid pointless and inappropriate function names, example: foo, bar (metasyntactic variable - placeholder variables)
			
			function foo(){
				console.log(25%5)
			}
			foo();